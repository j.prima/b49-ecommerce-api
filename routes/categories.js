const express = require("express");
const router = express.Router();
const Category = require("../models/Category");
const isAdmin = require("../auth");

// Retrieve
router.get("/", isAdmin, (req, res) => {
	Category.find({}, (err, categories) => {
		return res.json(categories)
	})
})

module.exports = router;