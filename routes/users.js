const express = require("express")
const router = express.Router();
const User = require("../models/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

// Register
router.post("/", (req, res) => {
	// Input checks
	if(!req.body.username || req.body.username.length < 6) return res.status(400).json({
		status: 400,
		message: "Username must be greater than 6 characters"
	})
	if(!req.body.password || req.body.password.length < 6) return res.status(400).json({
		status: 400,
		message: "Password must be greater than 6 characters"
	});
	if(!req.body.password || req.body.password != req.body.password2) return res.status(400).json({
		status: 400,
		message: "Password does not match"
	});

	// Check exist
	User.findOne({username: req.body.username}, (err, user) => {
		if(user) return res.status(400).json({
			status: 400,
			message: "Username already exists"
		});
		bcrypt.hash(req.body.password, 10, (err, hashedPassword) => {
			const user = new User();
			user.fullname = req.body.fullname;
			user.username = req.body.username;
			user.password = hashedPassword;
			user.save()
			return res.json({status: 200, message: "Registered Successfully."})
		})

	})
})

// Login
router.post("/login", (req, res) => {
	User.findOne({username: req.body.username}, (err, user) => {
		if(err || !user) return res.status(400).json({status: 400, message: "No user found."});
		bcrypt.compare(req.body.password, user.password, (err, result) => {
			if(!result) {
				return res.status(401).json({
					auth: false,
					message: "Invalid Credentials",
					token: null
				})
			} else {
				let token = jwt.sign(user.toJSON(), 'b49-ecommerce', {expiresIn: '1h'});
				return res.status(200).json({
					auth: true,
					message: "Logged in successfully.",
					user,
					token
				})
			}
		})
	})
})

module.exports = router;